import Axios from "axios";
import {
  CHANGE_DONE,
  DELETE_ITEM,
  HANDLE_NOTIFICATION,
  IN_EDIT_MODE,
  LOGIN,
  POST_ITEM,
  SAVE_EDIT,
  SET_DATA,
  START_ERROR_MODE,
} from "../Constants/Constants";

export function confirmLogin(jwt) {
  return { type: LOGIN, jwt };
}

export function LoginUser(user) {
  console.log(user);
  return function (dispatch) {
    return Axios({
      method: "post",
      url: "http://appointments.draft2017.com/auth/local",
      data: {
        identifier: user.user,
        password: user.password,
      },
    })
      .then((res) => {
        localStorage.setItem("jwt", res.data.jwt);
        dispatch(confirmLogin(res.data.jwt));
      })
      .catch((err) => {
        console.error(err);
        dispatch({
          type: START_ERROR_MODE,
          msg: err.response.data.message[0].messages[0].message,
        });
      });
  };
}

export function setData(data, ofType) {
  return {
    type: SET_DATA,
    data,
    ofType,
  };
}

export function postLocally(data, ofType) {
  return { type: POST_ITEM, data, ofType };
}

export function postItem(data) {
  return function (dispatch) {
    const dataToPost =
      data.ofType === "tasks"
        ? {
            Title: data.title,
            Description: data.desc,
            Done: data.done,
          }
        : {
            Title: data.title,
            Date: data.date,
            Time: data.time,
            Email: data.email,
            Hours: data.hours,
          };
    return Axios({
      method: "post",
      url: `http://appointments.draft2017.com/${data.ofType}`,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
      data: dataToPost,
    })
      .then((res) => {
        dispatch(
          postLocally(
            res.data,
            data.ofType === "tasks" ? "tasks" : "appointments"
          )
        );
        dispatch(
          handleNotification(
            `You have added a new ${
              data.ofType === "tasks" ? "task" : "appointment"
            }!`
          )
        );
      })
      .catch((err) => {
        console.log(err);
        dispatch(handleNotification("Something went wrong, please try again!"));
      });
  };
}

function deleteLocal(id, whichType) {
  return { type: DELETE_ITEM, id, whichType };
}

export function deleteItem(id, type) {
  return function (dispatch) {
    return Axios.delete(`http://appointments.draft2017.com/${type}/${id}`, {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
    })
      .then((res) => {
        dispatch(deleteLocal(res.data.id, type));
        dispatch(
          handleNotification(
            `You have removed a ${type === "tasks" ? "task" : "appointment"}!`
          )
        );
      })
      .catch((err) => {
        console.log(err);
        dispatch(handleNotification("Something went wrong, please try again!"));
      });
  };
}

function changeDoneLocal(id, item) {
  return {
    type: CHANGE_DONE,
    id,
    item,
  };
}

export function changeDone(id, done, num) {
  return function (dispatch) {
    return Axios({
      method: "put",
      url: "http://appointments.draft2017.com/tasks/" + id,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
      data: {
        Done: !done,
      },
    })
      .then((res) => {
        dispatch(changeDoneLocal(id, res.data));
        dispatch(
          handleNotification(
            `Task #${num} is ${done ? "incomplete" : "complete"}!`
          )
        );
      })
      .catch((err) => {
        console.log(err);
        handleNotification(`Something went wrong, please try again!`);
      });
  };
}

export function setEditMode(id, title, desc) {
  return {
    type: IN_EDIT_MODE,
    id,
    title,
    desc,
  };
}

export function saveChange(id, data, onWhat) {
  return {
    type: SAVE_EDIT,
    id,
    data,
    onWhat,
  };
}

export function saveEdit(data) {
  const itemToUpdate = data.ofType
    ? {
        Title: data.title,
        Description: data.desc,
      }
    : {
        Title: data.title,
        Date: data.date.toLocaleString().split(",")[0],
        Time: data.date.toLocaleString("en-US", {
          hour: "numeric",
          minute: "numeric",
          hour12: false,
        }),
        Email: data.email,
        Hours: parseInt(data.hour),
      };
  return function (dispatch) {
    return Axios({
      method: "put",
      url: `http://appointments.draft2017.com/${data.ofType}/` + data.editingID,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
      data: itemToUpdate,
    })
      .then((res) => {
        dispatch(saveChange(data.editingID, res.data, data.ofType));
        dispatch(handleNotification(`${data.ofType} edit successfull!`));
      })
      .catch((err) => {
        console.log(err);
        dispatch(handleNotification("Edit failed, please try again!"));
      });
  };
}

export function handleNotification(msg) {
  return {
    type: HANDLE_NOTIFICATION,
    msg,
  };
}
