import {
  CHANGE_DONE,
  DELETE_ITEM,
  HANDLE_FLIP,
  HANDLE_NOTIFICATION,
  HIDE_NOTIFICATION,
  IN_EDIT_MODE,
  LOGIN,
  LOGOUT,
  POST_ITEM,
  SAVE_EDIT,
  SET_DATA,
  START_ERROR_MODE,
  STOP_ERROR_MODE,
} from "../Constants/Constants";

const initialState = {
  isLoggedIn: false,
  tasks: [],
  appointments: [],
  jwt: undefined,
  inEditMode: false,
  inAppsEdit: false,
  editingID: -1,
  isFlipped: false,
  errorMode: false,
  errorMsg: "",
  showNotification: false,
  notificationMsg: "",
};

const rootReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        isLoggedIn: true,
        jwt: action.jwt,
      };

    case LOGOUT:
      localStorage.clear();
      return {
        ...state,
        isLoggedIn: false,
        tasks: [],
        appointments: [],
        jwt: undefined,
        inEditMode: false,
        inAppsEdit: false,
        editingID: -1,
        isFlipped: false,
        errorMode: false,
        errorMsg: "",
        showNotification: false,
        notificationMsg: "",
      };

    case START_ERROR_MODE:
      return {
        ...state,
        errorMode: true,
        errorMsg: action.msg,
      };

    case STOP_ERROR_MODE:
      return {
        ...state,
        errorMode: false,
        errorMsg: "",
      };

    case SET_DATA:
      return {
        ...state,
        [action.ofType]: action.data,
      };

    case POST_ITEM:
      return {
        ...state,
        [action.ofType]: [...state[action.ofType], action.data],
      };

    case DELETE_ITEM:
      return {
        ...state,
        [action.whichType]: state[action.whichType].filter(
          (el) => el.id !== action.id
        ),
      };

    case IN_EDIT_MODE:
      return {
        ...state,
        inEditMode: true,
        editingID: action.id,
      };

    case SAVE_EDIT:
      return {
        ...state,
        [action.onWhat]: state[action.onWhat].map((el) =>
          el.id === action.id ? action.data : el
        ),
        editingID: -1,
        inEditMode: false,
      };

    case CHANGE_DONE:
      return {
        ...state,
        tasks: state.tasks.map((el) =>
          el.id === action.id ? action.item : el
        ),
      };

    case HANDLE_FLIP:
      return {
        ...state,
        isFlipped: !state.isFlipped,
      };

    case HANDLE_NOTIFICATION:
      return {
        ...state,
        showNotification: true,
        notificationMsg: action.msg,
      };

    case HIDE_NOTIFICATION:
      return {
        ...state,
        showNotification: false,
        notificationMsg: "",
      };

    default:
      if (localStorage.getItem("jwt")) {
        return {
          ...state,
          isLoggedIn: true,
          jwt: localStorage.getItem("jwt"),
        };
      }
      break;
  }
  return state;
};

export default rootReducer;
