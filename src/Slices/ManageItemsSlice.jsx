import {
  createAsyncThunk,
  createSlice,
  isRejectedWithValue,
} from "@reduxjs/toolkit";
import Axios from "axios";

const initialState = {
  tasks: [],
  appointments: [],
  isFlipped: false,
  showNotification: false,
  notificationMsg: "",
  editingID: undefined,
  inEditMode: false,
};

export const fetchItems = createAsyncThunk("/getItems", async () => {
  const tasks = await Axios("http://appointments.draft2017.com/tasks", {
    headers: {
      Authorization: "Bearer " + localStorage.getItem("jwt"),
    },
  }).then((res) => res.data);

  const appointments = await Axios(
    "http://appointments.draft2017.com/appointments",
    {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
    }
  ).then((res) => res.data);

  const response = {
    tasks,
    appointments,
  };

  return response;
});

export const addNewItem = createAsyncThunk("/addNewItem", async (data) => {
  const dataToPost =
    data.ofType === "tasks"
      ? {
          Title: data.title,
          Description: data.desc,
          Done: data.done,
        }
      : {
          Title: data.title,
          Date: data.date,
          Time: data.time,
          Email: data.email,
          Hours: data.hours,
        };
  const postedItem = await Axios({
    method: "post",
    url: `http://appointments.draft2017.com/${data.ofType}`,
    headers: {
      Authorization: "Bearer " + localStorage.getItem("jwt"),
    },
    data: dataToPost,
  })
    .then((res) => res.data)
    .catch((err) => {
      throw new Error(err);
    });

  return postedItem;
});

export const deleteAnItem = createAsyncThunk("/deleteItem", async (data) => {
  const deletedItem = await Axios.delete(
    `http://appointments.draft2017.com/${data.type}/${data.id}`,
    {
      headers: {
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
    }
  );
  return deletedItem.data;
});

export const editAnItem = createAsyncThunk("/editAnItem", async (data) => {
  const itemToUpdate =
    data.ofType === "tasks"
      ? {
          Title: data.title,
          Description: data.desc,
        }
      : {
          Title: data.title,
          Date: data.date.toLocaleString().split(",")[0],
          Time: data.date.toLocaleString("en-US", {
            hour: "numeric",
            minute: "numeric",
            hour12: false,
          }),
          Email: data.email,
          Hours: parseInt(data.hours),
        };
  try {
    const editedItem = await Axios({
      method: "put",
      url: `http://appointments.draft2017.com/${data.ofType}/` + data.editingID,
      headers: {
        Authorization: "Bearer " + localStorage.getItem("jwt"),
      },
      data: itemToUpdate,
    });
    return editedItem.data;
  } catch (err) {
    if (!err.response) {
      throw err;
    }
    return isRejectedWithValue(err.response);
  }
});

export const markDone = createAsyncThunk("/markDone", async (data) => {
  const markedItem = await Axios({
    method: "put",
    url: "http://appointments.draft2017.com/tasks/" + data.id,
    headers: {
      Authorization: "Bearer " + localStorage.getItem("jwt"),
    },
    data: {
      Done: !data.done,
    },
  });
  return markedItem.data;
});

const ManageItemsSlice = createSlice({
  name: "getItems",
  initialState,
  reducers: {
    switchFlip: (state, action) => {
      state.isFlipped = !state.isFlipped;
    },

    hideNotification: (state, action) => {
      state.showNotification = false;
    },

    prepareEdit: (state, action) => {
      state.editingID = action.payload.id;
      state.inEditMode = true;
    },
    cancelEdit: (state, action) => {
      state.editingID = undefined;
      state.inEditMode = false;
    },
  },
  extraReducers: {
    [fetchItems.fulfilled]: (state, action) => {
      state.tasks = action.payload.tasks;
      state.appointments = action.payload.appointments;
    },

    [addNewItem.rejected]: (state, action) => {
      console.log(action);
      let ofType = action.meta.arg.ofType;
      state.showNotification = true;
      state.notificationMsg = `Something went wrong, please try again!`;
    },

    [addNewItem.fulfilled]: (state, action) => {
      const ofType = action.meta.arg.ofType;
      state[ofType] = [...state[ofType], action.payload];
      state.showNotification = true;
      state.notificationMsg = `Added a new ${ofType.substring(
        0,
        ofType.length - 1
      )}!`;
    },

    [deleteAnItem.fulfilled]: (state, action) => {
      const ofType = action.meta.arg.type;
      state[ofType] = state[ofType].filter((el) => el.id !== action.payload.id);
      state.showNotification = true;
      state.notificationMsg = `Succesfully removed ${ofType.substring(
        0,
        ofType.length - 1
      )}!`;
    },

    [editAnItem.rejected]: (state, action) => {
      console.log(action);
      state.showNotification = true;
      state.notificationMsg = `Something went wrong, please try again!`;
      state.editingID = undefined;
      state.inEditMode = false;
    },

    [editAnItem.fulfilled]: (state, action) => {
      const ofType = action.meta.arg.ofType;
      state[ofType] = state[ofType].map((el) =>
        el.id === action.payload.id ? action.payload : el
      );
      state.showNotification = true;
      state.notificationMsg = `${
        ofType
          .substring(0, ofType.length - 1)
          .charAt(0)
          .toUpperCase() + ofType.slice(1, ofType.length - 1)
      } has been edited succesfully!`;
      state.editingID = undefined;
      state.inEditMode = false;
    },

    [markDone.fulfilled]: (state, action) => {
      const num = action.meta.arg.num;
      const done = action.meta.arg.done;
      state.tasks = state.tasks.map((el) =>
        el.id === action.payload.id ? action.payload : el
      );
      state.showNotification = true;
      state.notificationMsg = `Task #${num} is now ${
        done ? "incomplete" : "complete"
      }!`;
    },
  },
});

export default ManageItemsSlice.reducer;
export const {
  switchFlip,
  hideNotification,
  prepareEdit,
  cancelEdit,
} = ManageItemsSlice.actions;
