import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import Axios from "axios";

const initialState = {
  isLoggedInTK: false,
  jwtTK: undefined,
  inErrorMode: false,
};
const loginSlice = createSlice({
  name: "login",
  initialState,
  reducers: {
    login: (state, { data }) => {
      console.log(data);
      state.isLoggedIn = true;
      state.jwt = data;
    },
    inError: (state) => {
      state.inErrorMode = true;
      state.isLoggedIn = false;
      state.jwt = undefined;
    },
  },
});

export const { login, inError } = loginSlice.actions;
export default loginSlice.reducer;

export const plsLogin = (data) => async (dispatch) => {
  try {
    Axios({
      method: "post",
      url: "http://appointments.draft2017.com/auth/local",
      data: {
        identifier: data.user,
        password: data.password,
      },
    }).then((res) => {
      localStorage.setItem("jwt", res.data.jwt);
      //   dispatch(confirmLogin(res.data.jwt));
      dispatch(login(res.data.jwt));
      console.log("login");
    });
  } catch (error) {
    dispatch(inError);
    console.log("fail");

    throw error;
  }
};

export const LoginUserTK = createAsyncThunk(async (data, thunk) => {
  console.log(data);
  console.log(thunk);
  try {
    Axios({
      method: "post",
      url: "http://appointments.draft2017.com/auth/local",
      data: {
        identifier: data.user,
        password: data.password,
      },
    }).then((res) => {
      localStorage.setItem("jwt", res.data.jwt);
      //   dispatch(confirmLogin(res.data.jwt));
      thunk.dispatch(login, res.data.jwt);
    });
  } catch (error) {
    throw error;
  }
});
