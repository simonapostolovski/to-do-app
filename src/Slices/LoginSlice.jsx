import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import Axios from "axios";

const initialState = {
  isLoggedIn: false,
  jwt: undefined,
  errorMode: false,
  errorMsg: "",
};

export const tryLogin = createAsyncThunk("login/auth", async (data) => {
  const response = await Axios({
    method: "post",
    url: "http://appointments.draft2017.com/auth/local",
    data: {
      identifier: data.user,
      password: data.password,
    },
  });
  return response.data;
});

const LoginSlice = createSlice({
  name: "login",
  initialState,
  reducers: {
    Authenticated: (state, action) => {
      state.jwt = localStorage.getItem("jwt");
      state.isLoggedIn = true;
    },
    Logout: (state, action) => {
      state.isLoggedIn = false;
      state.jwt = undefined;
      state.errorMode = false;
      state.errorMsg = "";
      localStorage.clear();
    },
  },
  extraReducers: {
    [tryLogin.rejected]: (state, action) => {
      console.log(action);
      state.errorMode = true;
      state.errorMsg = "Incorrect credentials, try again";
    },
    [tryLogin.fulfilled]: (state, action) => {
      state.isLoggedIn = true;
      state.jwt = action.payload.jwt;
      state.errorMode = false;
      state.errorMsg = "";
      localStorage.setItem("jwt", action.payload.jwt);
    },
  },
});

export const { Authenticated, Logout } = LoginSlice.actions;
export default LoginSlice.reducer;
