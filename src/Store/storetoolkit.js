import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
const { default: rootReducer } = require("../Reducers/RootReducer");

const reducer = combineReducers({ rootReducer });

const storeTK = configureStore({
  reducer,
});

export default storeTK;
