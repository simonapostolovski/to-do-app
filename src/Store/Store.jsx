const { createStore, applyMiddleware } = require("redux");
const { default: thunk } = require("redux-thunk");
const { default: rootReducer } = require("../Reducers/RootReducer");


export const store = createStore(rootReducer, applyMiddleware(thunk))
