import { configureStore } from "@reduxjs/toolkit";
import LoginSlice from "../Slices/LoginSlice";
import ManageItemsSlice from "../Slices/ManageItemsSlice";

const NewStore = configureStore({
  reducer: {
    LoginSlice,
    ManageItemsSlice,
  },
});

export default NewStore;
