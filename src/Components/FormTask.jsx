import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Tooltip from "react-tooltip-lite";
import { saveChange } from "../Actions/Actions";
import { addNewItem, cancelEdit, editAnItem } from "../Slices/ManageItemsSlice";
import "../Styles/Homepage.css";
const FormTask = () => {
  const { tasks, inEditMode, editingID, isFlipped } = useSelector((state) => ({
    tasks: state.ManageItemsSlice.tasks,
    inEditMode: state.ManageItemsSlice.inEditMode,
    editingID: state.ManageItemsSlice.editingID,
    isFlipped: state.ManageItemsSlice.isFlipped,
  }));

  const dispatch = useDispatch();
  const [title, setTitle] = useState("");
  const [desc, setDesc] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (inEditMode) {
      dispatch(editAnItem({ editingID, title, desc, ofType: "tasks" }));
      setTitle("");
      setDesc("");
    } else {
      console.log("submited");
      if (!title || !desc) {
        return;
      } else {
        dispatch(addNewItem({ title, desc, done: false, ofType: "tasks" }));
        setTitle("");
        setDesc("");
      }
    }
  };

  useEffect(() => {
    if (inEditMode && !isFlipped) {
      const foundEl = tasks.find((el) => el.id === editingID);
      setTitle(foundEl.Title);
      setDesc(foundEl.Description);
    }

    if (!inEditMode) {
      setTitle("");
      setDesc("");
    }
  }, [inEditMode, editingID, tasks, isFlipped]);

  return (
    <div className="tasks">
      <h2>Add a new task</h2>
      <form onSubmit={(e) => handleSubmit(e)}>
        <label htmlFor="title">Task Title</label>
        <input
          id="title"
          type="text"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
        <label htmlFor="desc">Task Description</label>
        <input
          id="desc"
          type="text"
          value={desc}
          onChange={(e) => setDesc(e.target.value)}
        />
        <div className="form-button">
          <Tooltip
            content={<p>Each field must be populated!</p>}
            direction={"down"}
            background="black"
            color="white"
            useHover={!title || !desc ? true : false}
            tipContentClassName="tooltip-border"
          >
            <button
              type="submit"
              className={!title || !desc ? "disabled-btn" : "submit-btn"}
            >
              {inEditMode ? "Update" : "Add a new task"}
            </button>
          </Tooltip>
          {inEditMode && (
            <button
              className="cancel-edit"
              onClick={() => {
                dispatch(cancelEdit());
                setTitle("");
                setDesc("");
              }}
            >
              Cancel
            </button>
          )}
        </div>
      </form>
    </div>
  );
};

export default FormTask;
