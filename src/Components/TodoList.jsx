import React from "react";
import { useSelector } from "react-redux";
import Item from "./Item";

const TodoList = ({ type }) => {
  const { tasks } = useSelector((state) => ({
    tasks: state.ManageItemsSlice.tasks,
  }));

  return (
    <>
      {!tasks.length ? (
        <h1>You dont have any tasks at the moment!</h1>
      ) : (
        <ul>
          {tasks &&
            tasks.map((el, i) => {
              return (
                <Item
                  key={el.id}
                  title={el.Title}
                  desc={el.Description}
                  done={el.Done}
                  id={el.id}
                  type={type}
                  num={i + 1}
                />
              );
            })}
        </ul>
      )}
    </>
  );
};

export default TodoList;
