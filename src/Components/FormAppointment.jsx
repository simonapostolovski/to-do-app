import React, { useEffect, useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import Tooltip from "react-tooltip-lite";
import {
  handleNotification,
  postItem,
  saveChange,
  saveEdit,
} from "../Actions/Actions";
import { cancelEdit, editAnItem } from "../Slices/ManageItemsSlice";
import "../Styles/Homepage.css";

const FormAppointment = () => {
  const { appointments, inEditMode, editingID, isFlipped } = useSelector(
    (state) => ({
      appointments: state.ManageItemsSlice.appointments,
      inEditMode: state.ManageItemsSlice.inEditMode,
      editingID: state.ManageItemsSlice.editingID,
      isFlipped: state.ManageItemsSlice.isFlipped,
    })
  );

  const dispatch = useDispatch();
  const [title, setTitle] = useState("");
  const [date, setDate] = useState("");
  const [email, setEmail] = useState("");
  const [hours, setHours] = useState("");

  useEffect(() => {
    if (inEditMode && isFlipped) {
      const foundEl = appointments.find((el) => el.id === editingID);
      console.log(foundEl);
      setTitle(foundEl.Title);
      setDate(new Date());
      setEmail(foundEl.Email);
      setHours(foundEl.Hours);
    }

    if (!inEditMode) {
      setTitle("");
      setDate("");
      setEmail("");
      setHours("");
    }
  }, [inEditMode, editingID, appointments, isFlipped]);

  return (
    <div className="tasks appointments">
      <h2>Add a new appointment</h2>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          if (inEditMode) {
            dispatch(
              editAnItem({
                editingID,
                title,
                date,
                email,
                hours,
                ofType: "appointments",
              })
            );
          } else {
            if (parseInt(hours) < 0) {
              toast("Hours must be positive values only!");
              return;
            } else {
              dispatch(
                postItem({
                  ofType: "appointments",
                  title,
                  date: date.toLocaleString().split(",")[0],
                  time: date.toLocaleString("en-US", {
                    hour: "numeric",
                    minute: "numeric",
                    hour12: false,
                  }),
                  email,
                  hours: parseInt(hours),
                })
              );
            }
          }
          setTitle("");
          setDate("");
          setEmail("");
          setHours("");
        }}
      >
        <label htmlFor="title">Appointment Title</label>
        <input
          type="text"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
        <label htmlFor="Email">Your Email</label>
        <input
          id="Email"
          type="text"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <p>When is the appointment?</p>
        <DatePicker
          selected={date}
          onChange={setDate}
          showTimeSelect
          dateFormat="yyyy-MM-dd, HH:mm"
          timeFormat="HH:mm"
          placeholderText="Click to select a date"
        />
        <label htmlFor="hours">How long will it last in hours?</label>
        <input
          min="0"
          id="hours"
          type="number"
          value={hours}
          onChange={(e) => setHours(e.target.value)}
        />
        <div className="form-button">
          <Tooltip
            content={<p>Each field must be populated!</p>}
            direction={"down"}
            background="black"
            color="white"
            useHover={!title || !date || !email || !hours ? true : false}
            tipContentClassName="tooltip-border"
          >
            <button
              type="submit"
              className={
                !title || !date || !email || !hours
                  ? "disabled-btn"
                  : "submit-btn"
              }
            >
              {inEditMode ? "Update" : "Add a new task"}
            </button>
          </Tooltip>
          {inEditMode && (
            <button
              className="cancel-edit"
              onClick={() => {
                dispatch(cancelEdit());
                setTitle("");
                setDate("");
                setEmail("");
                setHours("");
              }}
            >
              Cancel
            </button>
          )}
        </div>
      </form>
    </div>
  );
};

export default FormAppointment;
