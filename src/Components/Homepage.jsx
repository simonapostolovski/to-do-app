import React, { useEffect } from "react";
import ReactCardFlip from "react-card-flip";
import { useDispatch, useSelector } from "react-redux";
import { Logout } from "../Slices/LoginSlice";
import {
  cancelEdit,
  fetchItems,
  hideNotification,
  switchFlip,
} from "../Slices/ManageItemsSlice";
import "../Styles/Homepage.css";
import Appointments from "./Appointments";
import FormAppointment from "./FormAppointment";
import FormTask from "./FormTask";
import TodoList from "./TodoList";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
const Homepage = () => {
  const dispatch = useDispatch();

  const {
    jwt,
    isFlipped,
    showNotification,
    notificationMsg,
    inEditMode,
  } = useSelector((state) => ({
    jwt: state.LoginSlice.jwt,
    isFlipped: state.ManageItemsSlice.isFlipped,
    showNotification: state.ManageItemsSlice.showNotification,
    notificationMsg: state.ManageItemsSlice.notificationMsg,
    inEditMode: state.ManageItemsSlice.notificationMsg,
  }));

  useEffect(() => {
    if (showNotification) {
      toast(notificationMsg);
      setTimeout(() => {
        dispatch(hideNotification());
      }, 2600);
    }
  }, [showNotification]);

  useEffect(() => {
    if (jwt) {
      dispatch(fetchItems());
    }
  }, []);

  return (
    <>
      <button
        onClick={() => {
          dispatch(cancelEdit());
          dispatch(switchFlip());
        }}
        className="switch-view"
      >
        {isFlipped ? "See your tasks" : "See your appointments"}
      </button>
      <button onClick={() => dispatch(Logout())} className="logout-btn">
        LOGOUT
      </button>

      {showNotification && (
        <ToastContainer
          position="top-center"
          autoClose={2400}
          hideProgressBar={false}
          newestOnTop={false}
          rtl={false}
          draggable
        />
      )}

      <div className="main-wrapper">
        <div className="content-wrapper">
          <ReactCardFlip isFlipped={isFlipped} flipDirection="horizontal">
            <div className="card front">
              <TodoList type="tasks" />
            </div>
            <div className="card back">
              <Appointments type="appointments" />
            </div>
          </ReactCardFlip>
        </div>
        <div className="form-wrapper">
          <ReactCardFlip isFlipped={isFlipped} flipDirection="vertical">
            <div className="formHandle front">
              <FormTask />
            </div>
            <div className="formHandle back">
              <FormAppointment />
            </div>
          </ReactCardFlip>
        </div>
      </div>
    </>
  );
};

export default Homepage;
