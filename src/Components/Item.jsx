import React from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteAnItem,
  markDone,
  prepareEdit,
} from "../Slices/ManageItemsSlice";
import "../Styles/Item.css";
const Item = ({ title, desc, done, id, type, num, hours, time, date }) => {
  const { showNotification } = useSelector((state) => ({
    showNotification: state.ManageItemsSlice.showNotification,
  }));
  const dispatch = useDispatch();
  return (
    <>
      <li className="item-main">
        <div
          className={done ? "item-content done-item" : "item-content"}
          onClick={() =>
            showNotification ? null : dispatch(markDone({ id, done, num }))
          }
        >
          {date && (
            <h3>
              Appointment #{num}: {title} on {date} at {time}h for {hours}
              hours.
            </h3>
          )}
          {desc && (
            <>
              <h3>
                Task #{num}: {title}.
              </h3>
              <p>
                Task Description: <span>{desc}.</span>
              </p>
            </>
          )}
        </div>
        <button
          disabled={showNotification}
          onClick={() => dispatch(prepareEdit({ id }))}
          className="edit-btn"
        >
          Edit
        </button>
        <button
          disabled={showNotification}
          onClick={() => dispatch(deleteAnItem({ id, type }))}
          className="del-btn"
        >
          Delete
        </button>
      </li>
    </>
  );
};

export default Item;
