import React from "react";
import { useSelector } from "react-redux";
import Item from "./Item";
const Appointments = ({ type }) => {
  const { appointments } = useSelector((state) => ({
    appointments: state.ManageItemsSlice.appointments,
  }));

  return (
    <>
      {!appointments.length ? (
        <h1>You dont have any appointments at the moment!</h1>
      ) : (
        <ul>
          {appointments &&
            appointments.map((el, i) => {
              return (
                <Item
                  key={el.id}
                  title={el.Title}
                  time={el.Time}
                  hours={el.Hours}
                  id={el.id}
                  type={type}
                  num={i + 1}
                  date={el.Date}
                />
              );
            })}
        </ul>
      )}
    </>
  );
};

export default Appointments;
