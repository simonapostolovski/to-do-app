import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { STOP_ERROR_MODE } from "../Constants/Constants";
import { tryLogin } from "../Slices/LoginSlice";
import "../Styles/Login.css";
const Login = () => {
  const { errorMode, errorMsg } = useSelector((state) => ({
    errorMode: state.LoginSlice.errorMode,
    errorMsg: state.LoginSlice.errorMsg,
  }));

  console.log(useSelector((state) => state));
  const dispatch = useDispatch();
  const [user, setUser] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    const sendUser = {
      user,
      password,
    };
    // dispatch(authNow(sendUser));
    dispatch(tryLogin(sendUser));
    // dispatch(LoginUser(sendUser));
  };

  return (
    <div className="login-wrapper">
      <h1>Please Log In Before You Continue</h1>

      <form
        className={errorMode ? "login-error" : "login-form"}
        onSubmit={(e) => handleSubmit(e)}
      >
        {errorMode && <p className="errorMsg">{errorMsg}</p>}
        <label htmlFor="user">Username</label>
        <input
          className={errorMode ? "inputErr" : ""}
          type="text"
          name="user"
          id="user"
          required
          onFocus={() =>
            errorMode
              ? dispatch({
                  type: STOP_ERROR_MODE,
                })
              : null
          }
          onChange={(e) => {
            setUser(e.target.value);
          }}
        />
        <label htmlFor="pass">Password</label>
        <input
          className={errorMode ? "inputErr" : ""}
          type="password"
          name="pass"
          id="pass"
          required
          onFocus={() =>
            errorMode
              ? dispatch({
                  type: STOP_ERROR_MODE,
                })
              : null
          }
          onChange={(e) => {
            setPassword(e.target.value);
          }}
        />
        <button type="submit">Log In</button>
      </form>
    </div>
  );
};

export default Login;
