import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter, Redirect, Route } from "react-router-dom";
import Homepage from "./Components/Homepage";
import { Authenticated } from "./Slices/LoginSlice";

import Login from "./Components/Login";
const App = () => {
  const { isLoggedIn } = useSelector((state) => ({
    isLoggedIn: state.LoginSlice.isLoggedIn,
  }));

  const dispatch = useDispatch();
  useEffect(() => {
    if (localStorage.getItem("jwt")) {
      dispatch(Authenticated());
    }
  }, []);

  return (
    <>
      <BrowserRouter>
        {isLoggedIn ? <Redirect to="/" /> : <Login />}
        <Route exact path="/login">
          <Login />
        </Route>
        {isLoggedIn && (
          <Route exact path="/">
            <Homepage />
          </Route>
        )}
      </BrowserRouter>
    </>
  );
};

export default App;
